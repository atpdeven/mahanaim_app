import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mahanaim_app/component/login_berhasil.dart';
import 'package:mahanaim_app/navigation/pengaturan.dart';

class AuthPage extends StatelessWidget {
  AuthPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot){
          //If logged in
          if(snapshot.hasData){
            return LoginBerhasil();
          }

          //Not Login
          else{
            return Pengaturan();
          }
        },
      ),
    );
  }
}
