import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginBerhasil extends StatelessWidget {
  LoginBerhasil({super.key});

  final user = FirebaseAuth.instance.currentUser!;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Berhasil Login"),
        actions: [IconButton(onPressed: signUserOut, icon: Icon(Icons.logout))],
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Berhasil Masuk",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 25,
          ),
          Text(
            "Admin Permata",
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 25,
          ),
          GestureDetector(
            onTap: signUserOut,
            child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.symmetric(horizontal: 125),
              decoration: BoxDecoration(
                  color: Colors.red, borderRadius: BorderRadius.circular(9)),
              child: Center(
                child: Text(
                  "Keluar",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )
        ],
      )),
    );
  }

  void signUserOut() async {
    await FirebaseAuth.instance.signOut();
  }
}
