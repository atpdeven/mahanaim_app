import 'package:gsheets/gsheets.dart';
import 'package:mahanaim_app/model/sheet_model.dart';

class SheetsFlutter {
  static String _sheetId = "12gfpF0XqgGSX3CMP-3D-l6wbaYQLBRf0pLi9SkjwJo0";
  static const _sheetCredentials = r'''
  {
  "type": "service_account",
  "project_id": "database-mahanaim",
  "private_key_id": "8604dbe66ca9010be5edc3fe308a20a76e0f30e7",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCm2Df0Z3hBJOyh\nbfQY3fogVXsFWEHQ4vCO0wIlZzDYDUkjjpay+uy+mge09JIqU/zIxs84GpxzyeGc\nxNGUgs1zSqDueBRAjmbfUlKMjZ7ZFr+FRmtte64Rp7aRB4QaTSCizhNyYqbwDB+F\nyo+IYGQSQvQ12XycPaSNRqa9Dphz9OqdImZ8kDy2TrsWTh5AY1l+GojKiFcqvtq1\nk0rd4tAXZFofRedukbYvV8hif9ayAzDWnDVxjlMMsMY/OnnMNjYSZf+j3/rBJK3b\nFcZlXmT3VHn/ZPQ2VT7j8e6Plw4Db/pYwHG9YZo30GIT8/xcP9RhXt1eZSiPHZza\nHKQz3EFDAgMBAAECggEAAo7GcsF2ig8OXh8qdSdDSmU3p85y+tCS6V8nUNl5MIGM\nqozYfmH7eUFzLJoutJubAK13Ks1/cJErLAau+TzL139+QS7Ilh2rUuTpsrrfpLCm\nKoamILH14gw+Xy2SvPas4ttV9QxWb8fT7QtuVA9NtlTsXa3OdyHuRQzQ5mQ4NiZS\ndQF2fbtZXdvNO/cqhcIsmg4HY5V2ZjhoZfOKmDZOgUxnP20pEu6/9mOncekRCS8A\npNkUYsbgutHwm6uNv5bZwFZ0n6P1N8mnVGkSaXJaXI2rEcvTgDg0ydQV61JfcUo2\nYjbwLbkAJ+JUP1GGuCWnSNL1Zy8LqsLJ4y7Fhgw2GQKBgQDbDPX4wSVO6nz0ZM9B\nH0p8rGH7HgQU6gt9dHZ7Z/MDZIouEXwUQBhnEVOVoi1OqG8UdFZdPdx3zVEVSViR\n8RfXxWjr+tZZO1ueUvjT/pMiF1OXi4BG2+Q8P04u1YIb28YkVJLxmbIkyspVBsGc\nLAHEs826FInpFlxr0lGMFs0EDQKBgQDC/OX9ziZaKPZc18d3dJQwNvL/NAvxv5hN\nyK+UnQ3sLogRHta1hq6wd8P2i/K6n3ppNattwvYU6UJDRTnuBnsMVgbZ//fUmS78\npKPAtpvd0GMGNocLuCM1qWPKZiSEMlaUmka/6VJP5D+6OMs1vDOWGm5vLUT1FYr3\nRTC7Ch12jwKBgQC7BGDIxZZlvvt56MuuDNoagU6SCDlx5bRzosg5ajFnVyJGXLUz\n0NlEvd0/5puKCXH047KMnvEo84ljhCBzUYSKusdumdUBkBUTokFBNZtGwuWif/7A\nAKLm8SF+CNmxkTyk7fQbA569nMmMOF16SXQlDWH6meh4sZeskn7+/MSCzQKBgDW/\nYmJIvfeiTO6gqCH8Z5C2xNql7RjxODlQtV1gwWK2HNmjR4n4GxUXnlPKldjtdt+j\nZKK31/9BoPtMDMDY1ng7Cl5zGUxoATSw6VaIv4QwGeuvgiAv3LzA+XnkF/K8sXL6\n57+kwAS3pP1fBFzGiO26UXCh6N0p2Al68AjOq/pvAoGBAJGpfxLk7naW6IpxASoY\nKj3cTTFETxoD1Z/gNzzAwlyO+IHYeFn+2Ez06LMXaWZaLwEvco/eMGHrVnv17io5\nM9GmHpXybr6130K3AkFGGhWKfI/IZ4OLCIxHycufg3rsA8Hg2p/YcYkVS1TYhQtN\njJU5r1bZdDISybstTaU/IHGt\n-----END PRIVATE KEY-----\n",
  "client_email": "fluttersheets@database-mahanaim.iam.gserviceaccount.com",
  "client_id": "117815878278541525772",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/fluttersheets%40database-mahanaim.iam.gserviceaccount.com",
  "universe_domain": "googleapis.com"
  }
''';

  static Worksheet? _userSheet;
  static final _gsheets = GSheets(_sheetCredentials);

  static Future init() async {
    try {
      final spreadsheet = await _gsheets.spreadsheet(_sheetId);

      _userSheet =
          await _getWorkSheet(spreadsheet, title: "BazzarPermataRunggun");
      final firstRow = SheetsColumn.getColumns();
      _userSheet!.values.insertRow(1, firstRow);
    } catch (e) {
      print(e);
    }
  }

  static Future<Worksheet> _getWorkSheet(
    Spreadsheet spreadsheet, {
    required String title,
  }) async {
    try {
      return await spreadsheet.addWorksheet(title);
    } catch (e) {
      return spreadsheet.worksheetByTitle(title)!;
    }
  }

  static Future insert(List<Map<String, dynamic>> rowList) async {
    _userSheet!.values.map.appendRows(rowList);
  }
}
