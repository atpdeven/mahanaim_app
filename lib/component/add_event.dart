import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddEventScreen extends StatefulWidget {
  const AddEventScreen({Key? key}) : super(key: key);

  @override
  State<AddEventScreen> createState() => _AddEventScreenState();
}

class _AddEventScreenState extends State<AddEventScreen> {
  String selectedTitle = 'Option 1'; // Default selected title

  final TextEditingController dateController = TextEditingController();

  DateTime? _selectedDate;

  @override
  void initState() {
    super.initState();
    dateController.text =
        _selectedDate?.toIso8601String().substring(0, 10) ?? "Pilih Tanggal";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Tambah Event")),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            // Radio buttons for title selection
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Pilih Nama Event',
                  style: TextStyle(fontSize: 16.0),
                ),
                RadioListTile(
                  title: const Text('PA Gabungan'),
                  value: 'PA Gabungan',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                RadioListTile(
                  title: const Text('PA Sektor'),
                  value: 'PA Sektor',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                RadioListTile(
                  title: const Text('Acara Kebersamaan'),
                  value: 'Acara Kebersamaan',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                RadioListTile(
                  title: const Text('Wisuda'),
                  value: 'Wisuda',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                RadioListTile(
                  title: const Text('Ngapuli'),
                  value: 'Ngapuli',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                RadioListTile(
                  title: const Text('Kunjungan Kasih'),
                  value: 'Kunjungan Kasih',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                RadioListTile(
                  title: const Text('Latihan VG'),
                  value: 'Latihan VG',
                  groupValue: selectedTitle,
                  onChanged: (value) {
                    setState(() {
                      selectedTitle = value.toString();
                    });
                  },
                ),
                // Add more RadioListTile for additional choices as needed
              ],
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              controller: dateController,
              readOnly: true,
              onTap: () async {
                DateTime? newDate = await showDatePicker(
                  context: context,
                  initialDate:
                      _selectedDate != null ? _selectedDate! : DateTime.now(),
                  firstDate: DateTime(2020),
                  lastDate: DateTime(2030),
                );
                if (newDate != null) {
                  setState(() {
                    _selectedDate = newDate;
                    dateController.text =
                        newDate.toIso8601String().substring(0, 10);
                  });
                }
              },
            ),
            SizedBox(
              height: 16,
            ),
            ElevatedButton(
              onPressed: () {
                if (_selectedDate != null) {
                  FirebaseFirestore.instance.collection('events').add({
                    'title': selectedTitle,
                    'date': _selectedDate,
                  });
                  Navigator.pop(context);
                } else {
                  // Handle case when date is not selected
                }
              },
              child: Text("Tambah Event"),
            ),
          ],
        ),
      ),
    );
  }
}
