import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mahanaim_app/provider/cart_provide.dart';
import 'package:mahanaim_app/component/googlesheets.dart';
import 'package:mahanaim_app/model/sheet_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class MyCart extends StatefulWidget {
  const MyCart({super.key});

  @override
  State<MyCart> createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
  List<CartItem> cartItemsSaved = List.empty(growable: true);

  final TextEditingController nama = TextEditingController();
  final TextEditingController sektor = TextEditingController();

  late SharedPreferences sp;
  Text errorMessage = Text(
    'Nama Harus Diisi*',
    style: TextStyle(fontSize: 8),
  );

  getSharedPreferences() async {
    sp = await SharedPreferences.getInstance();
    bool CheckValue = sp.containsKey('value');

    print(CheckValue);
    readFromSp();
  }

  @override
  void initState() {
    getSharedPreferences();
    super.initState();
  }

  void saveIntoCardItem(List<CartItem> cartItems) {
    // cartItems.clear();
    cartItemsSaved.addAll(cartItems);
    cartItems.clear();
    saveIntoSp();

    // print(cartItemsSave);
  }

  //Save Data
  saveIntoSp() {
    List<String> cartItemListString = cartItemsSaved
        .map((cartItemsSave) => jsonEncode(cartItemsSave.toJson()))
        .toList();
    sp.setStringList('myData', cartItemListString);
  }

  //GetData
  readFromSp() {
    try {
      List<String>? cartItemListString = sp.getStringList('myData');
      if (cartItemListString != null) {
        cartItemsSaved = cartItemListString
            .map((cartItemSave) => CartItem.fromJson(json.decode(cartItemSave)))
            .toList();
      }
      setState(() {});

      print(cartItemsSaved);
    } catch (e) {
      print('Error reading data from SharedPreferences: $e');
    }

    // print(cartItemsSaved);
  }

  void showCheckoutDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Terima Kasih"),
            content: Text("Kamu Berhasil Berbelanja Di Lapak Permata"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Ok"))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    List<CartItem> cardItems = Provider.of<CartProvider>(context).cartItems;
    List<CartItem> allItems = [...cardItems, ...cartItemsSaved];
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Text(
              "Keranjang",
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            ),
            Expanded(
              child: ListView.builder(
                // itemCount: productNames.length,
                itemCount: allItems.length,
                itemBuilder: (context, index) {
                  CartItem item = allItems[index];
                  // print(item.image);
                  return Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.only(
                      right: 16.0,
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8)),
                    child: Row(
                      children: [
                        Image.network(
                          item.image,
                          height: 50,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          children: [
                            Text(
                              // productNames[index],
                              item.name,
                              style: TextStyle(fontSize: 18),
                            ),
                            Text(
                              // "Rp ${prices[index]}",
                              "Rp ${item.price}",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            )
                          ],
                        ),
                        Spacer(),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                decrementQuantity(item);
                              },
                              icon: Icon(Icons.remove),
                            ),
                            Text(
                              item.quantity.toString(),
                              style: TextStyle(fontSize: 18),
                            ),
                            IconButton(
                              onPressed: () {
                                incrementQuantity(item);
                              },
                              icon: Icon(Icons.add),
                            ),
                            IconButton(
                                onPressed: () {
                                  deleteItem(item);
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                ))
                          ],
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      saveIntoCardItem(cardItems);
                    });
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text("Barang Berhasil Disimpan"),
                      ),
                    );
                  },
                  child: Text(
                    "Simpan",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                )
              ],
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                children: [
                  Text(
                    "Total Belanja : ",
                    style: TextStyle(fontSize: 18),
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Text(
                    "Rp ${getCartTotal(allItems)}",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.green,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                      ),
                      onPressed: () {
                        openDialog(allItems);
                      },
                      child: Text(
                        "Pesan Sekarang",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future openDialog(List<CartItem> allItems) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Data Anda"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: nama,
                decoration: InputDecoration(
                  hintText: "Nama Kamu",
                  errorText: errorMessage.data,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                controller: sektor,
                decoration: InputDecoration(hintText: "Sektor Permata"),
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () async {
                if (nama.text.trim().isEmpty) {
                  setState(() {
                    // Set error message text
                    errorMessage = Text('Nama harus diisi.');
                  });
                } else {
                  String message = "";
                  var nomor = 1;
                  int totalItems = allItems.length;
                  for (int i = 0; i < totalItems; i++) {
                    CartItem item = allItems[i];
                    // Append item details to the message string
                    message +=
                        "${nomor} : ${item.name} Jumlah: ${item.quantity}";
                    // Check if the current item is not the last item
                    if (i < totalItems - 1) {
                      // Append newline character if it's not the last item
                      message += ",\n";
                    }
                    nomor++;
                  }

                  DateTime now = DateTime.now();
                  String formattedDate =
                      DateFormat('yyyy-MM-dd – kk:mm').format(now);

                  final feedback = {
                    SheetsColumn.nama: nama.text.trim(),
                    SheetsColumn.permata: sektor.text.trim(),
                    SheetsColumn.belanja: message,
                    SheetsColumn.total: "Rp ${getCartTotal(allItems)}",
                    SheetsColumn.lunas: "Belum",
                    SheetsColumn.waktuPesan:formattedDate,
                  };

                  await SheetsFlutter.insert([feedback]);
                  buyAll(allItems);
                  Navigator.pop(context);
                }
              },
              child: Text("Pesan Barang"),
            )
          ],
        ),
      );

  void buyAll(List<CartItem> allItems) async {
    // Assuming WhatsApp number and format are correct
    var whatsappNumber = "+6283829554927";
    var nomor = 1;

    // Constructing the message
    String message =
        "Syalom Kak, Saya mau beli Barang dari Lapak Permata, \nNama : ${nama.text}\nSektor : ${sektor.text} \ningin membeli \n";
    for (CartItem item in allItems) {
      message +=
          "${nomor} : *${item.name}*, Harga: Rp ${item.price}, Jumlah: ${item.quantity}\n";
      nomor++;
    }
    message += "Total Belanja: *Rp ${getCartTotal(allItems)}*";

    // Constructing the WhatsApp URI
    var whatsappUri = Uri.parse(
        "whatsapp://send?phone=$whatsappNumber&text=${Uri.encodeComponent(message)}%0aBisa dibayarkan kemana ya kak ?");

    // Checking if WhatsApp is installed and launching the URI
    if (await canLaunchUrl(whatsappUri)) {
      await launchUrl(whatsappUri);
    } else {
      // WhatsApp not installed, show a snackbar or handle the situation accordingly
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("WhatsApp belum terinstall"),
        ),
      );
    }
  }

  void incrementQuantity(CartItem item) {
    setState(() {
      item.quantity++;
    });
    saveIntoSp();
  }

  void decrementQuantity(CartItem item) {
    setState(() {
      if (item.quantity > 1) {
        item.quantity--;
      }
    });
    saveIntoSp();
  }

  int getCartTotal(List<CartItem> allItems) {
    int total = 0;
    for (int i = 0; i < allItems.length; i++) {
      total += allItems[i].price * allItems[i].quantity;
    }
    return total;
  }

  void deleteItem(CartItem item) {
    setState(() {
      // Remove the item from the list of saved cart items
      cartItemsSaved.remove(item);
      // If you want to delete from the provider as well, you can do so
      Provider.of<CartProvider>(context, listen: false).removeItem(item);
      // After removing the item, save the updated list into SharedPreferences
      saveIntoSp();
    });
  }
}
