import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:mahanaim_app/provider/cart_provide.dart';
import 'package:mahanaim_app/component/my_cart.dart';
// import 'package:mahanaim_app/component/cart_provide.dart';
// import 'package:mahanaim_app/component/my_cart.dart';
import 'package:mahanaim_app/model/product_model.dart';
import 'package:provider/provider.dart';
// import 'package:mahanaim_app/model/product_model.dart';
// import 'package:mahanaim_app/navigation/lapak_permata.dart';
// import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductDetail extends StatefulWidget {
  final CardItem cardItem;
  const ProductDetail({super.key, required this.cardItem});

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  int currentSlide = 0;
  int selectedButton = 2;

  void selectButton(int buttonIndex) {
    setState(() {
      selectedButton = buttonIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    CardItem cardItem = widget.cardItem;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyCart(),
                  ),
                );
              }),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          CarouselSlider(
            options: CarouselOptions(
                height: 200,
                enlargeCenterPage: true,
                onPageChanged: (index, _) {
                  setState(() {
                    currentSlide = index;
                  });
                }),
            items: cardItem.images.map((image) {
              return Builder(
                builder: (context) {
                  return Image.network(
                    image,
                    fit: BoxFit.cover,
                  );
                },
              );
            }).toList(),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      cardItem.title,
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    Text(
                      "Rp ${cardItem.pricing}",
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Text(
              'Deskripsi Produk',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(8)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  cardItem.desc.replaceAll("\\n", "\n"),
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
          Spacer(),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 60,
                  child: ElevatedButton(
                    onPressed: () {
                      selectButton(1);
                      autoBuy();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor:
                          selectedButton == 1 ? Colors.green : Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    child: Text(
                      "Beli Sekarang",
                      style: TextStyle(
                          color:
                              selectedButton == 1 ? Colors.white : Colors.black,
                          fontSize: 18),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 60,
                  child: ElevatedButton(
                    onPressed: () {
                      selectButton(2);
                      addtoCart();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor:
                          selectedButton == 2 ? Colors.green : Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    child: Text(
                      "Tambah Keranjang",
                      style: TextStyle(
                          color:
                              selectedButton == 2 ? Colors.white : Colors.black,
                          fontSize: 18),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  void autoBuy() async {
    var whatsapp = "+6283829554927";
    var whatsappAndroid = Uri.parse(
        "whatsapp://send/?phone=$whatsapp&text=Syalom Kak, Saya mau beli Barang dari Lapak Permata, %0aNama : %0aSektor : %0aingin membeli *${widget.cardItem.title}* harganya *Rp ${widget.cardItem.pricing}*, Bisa dibayarkan kemana ya kak ?");
    if (await canLaunchUrl(whatsappAndroid)) {
      await launchUrl(whatsappAndroid);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("WhatsApp Belum Terinstall"),
        ),
      );
    }
  }

  void addtoCart() {
    CartProvider cartProvider =
        Provider.of<CartProvider>(context, listen: false);
    // Check if the item is already in the cart
    bool itemExists = false;
    for (CartItem cartItem in cartProvider.cartItems) {
      if (cartItem.name == widget.cardItem.title) {
        // Item already exists in the cart, increase quantity
        cartItem.quantity++;
        itemExists = true;
        break;
      }
    }

    // If the item is not in the cart, add it
    if (!itemExists) {
      CartItem newItem = CartItem(
          name: widget.cardItem.title,
          price: int.parse(widget.cardItem.pricing),
          quantity: 1,
          image: widget.cardItem.images[0]);
      cartProvider.addtoCart(newItem);
    }

    if (!itemExists)
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("Barang berhasil ditambahkan"),
        ),
      );
  }
}
