import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
// import 'package:mahanaim_app/component/auth_page.dart';
import 'package:mahanaim_app/component/my_cart.dart';
import 'package:mahanaim_app/pages/home_screen.dart';
// import 'package:mahanaim_app/model/event_model.dart';
import 'package:mahanaim_app/widget/navbar.dart';
import 'package:mahanaim_app/navigation/database.dart';
import 'package:mahanaim_app/navigation/event.dart';
// import 'package:mahanaim_app/navigation/favorit.dart';
// import 'package:mahanaim_app/navigation/Lapak_Permata.dart';
import 'package:mahanaim_app/navigation/lapak_permata.dart';
// import 'package:mahanaim_app/navigation/pengaturan.dart';
import 'package:mahanaim_app/utilities/geo_location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedPage = 0;
  bool hasItemsInCart = false;
  bool isLoggedIn = false;

  @override
  void initState() {
    super.initState();
    // Check shared preferences when the widget initializes
    checkCartItems();

    FirebaseAuth.instance.authStateChanges().listen(
      (user) {
        if (user != null) {
          setState(() {
            isLoggedIn = true;
          });
        } else {
          setState(() {
            isLoggedIn = false;
          });
        }
      },
    );
  }

  void checkCartItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? cartData = prefs.getString('cart');
    if (cartData != null && jsonDecode(cartData).isNotEmpty) {
      // If cartData is not null and not empty, there are items in the cart
      setState(() {
        hasItemsInCart = true;
      });
    } else {
      setState(() {
        hasItemsInCart = false;
      });
    }
  }

  final _pageOptions = [
    Home_Screen(),
    EventScreen(),
    Lapak_Permata(),
    DatabasePermata()
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          floatingActionButton: SpeedDial(
            children: [
              SpeedDialChild(
                  child: Icon(Icons.location_city),
                  label: 'Rute ke GBKP Banpus / Tempat PA',
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LocationPage()),
                    );
                  }),
              SpeedDialChild(
                child: Icon(Icons.mail),
                label: 'Masukan dan Saran',
                onTap: () {
                  _launchWhatsappcomment();
                },
              ),
              SpeedDialChild(
                child: Icon(Icons.mail),
                label: 'Hubungi Kami',
                onTap: () {
                  _launchWhatsappask();
                },
              ),
              SpeedDialChild(
                child: Icon(Icons.pages_sharp),
                label: 'Instagram',
                onTap: () => launchUrl(
                  Uri.parse('https://www.instagram.com/pmahanaim_banpus/'),
                  mode: LaunchMode.externalApplication,
                ),
              )
            ],
            animatedIcon: AnimatedIcons.menu_close,
            overlayOpacity: 0.8,

            // child: Icon(Icons.add),
            backgroundColor: Colors.black87,
            foregroundColor: Colors.yellow,
            elevation: 2,
            shape: CircleBorder(),
            // shape: BeveledRectangleBorder(borderRadius: BorderRadius.circular(20)),
          ),
          backgroundColor: Colors.white,
          drawer: NavBar(
            isLoggedIn: isLoggedIn,
          ),
          appBar: AppBar(
            title: const Text("Mahanaim Banpus"),
            elevation: 0,
            actions: <Widget>[
              IconButton(
                  icon: Icon(
                    hasItemsInCart ? Icons.shopping_cart : Icons.shopping_bag,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyCart()),
                    );
                  }),
              SizedBox(
                width: 10,
              ),
              Icon(
                Icons.notifications,
                color: Colors.black,
              ),
              SizedBox(
                width: 10,
              ),
            ],
          ),
          body: _pageOptions[selectedPage],
          bottomNavigationBar: Container(
            color: Colors.black,
            // shape: CircularNotchedRectangle(),
            // notchMargin: 5.0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              child: GNav(
                backgroundColor: Colors.black,
                color: Colors.white,
                activeColor: Colors.white,
                tabBackgroundColor: Colors.grey.shade800,
                padding: EdgeInsets.all(10),
                onTabChange: (index) {
                  setState(() {
                    selectedPage = index;
                  });
                },
                gap: 8,
                tabs: [
                  GButton(
                    icon: Icons.home,
                    text: "Beranda",
                  ),
                  GButton(
                    icon: Icons.calendar_month,
                    text: "Jadwal",
                  ),
                  GButton(
                    icon: Icons.shopping_bag_rounded,
                    text: "Bazzar Permata",
                  ),
                  GButton(
                    icon: Icons.people,
                    text: "Database",
                  )
                ],
              ),
            ),
          )),
      onWillPop: () => _onBackButtonPressed(context),
    );
  }

  _launchWhatsappask() async {
    var whatsapp = "+6281296214780";
    var whatsappAndroid = Uri.parse(
        "whatsapp://send/?phone=$whatsapp&text=Hello Admin, Saya ingin bertanya Mengenai Mahanaim App");
    if (await canLaunchUrl(whatsappAndroid)) {
      await launchUrl(whatsappAndroid);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("WhatsApp Belum Terinstall"),
        ),
      );
    }
  }

  _launchWhatsappcomment() async {
    var whatsapp = "+6281296214780";
    var whatsappAndroid = Uri.parse(
        "whatsapp://send/?phone=$whatsapp&text=Hello Admin, Saya memberikan masukan untuk Mahanaim App");
    if (await canLaunchUrl(whatsappAndroid)) {
      await launchUrl(whatsappAndroid);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("WhatsApp Belum Terinstall"),
        ),
      );
    }
  }

  Future<bool> _onBackButtonPressed(BuildContext context) async {
    bool? exitApp = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Keluar Dari Aplikasi ?"),
            content: const Text(
                "Apakah Anda Yakin ingin Keluar dari Mahanaim_App ?"),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    if (Platform.isAndroid || Platform.isIOS) {
                      // final appId = Platform.isAndroid
                      //     ? 'YOUR_ANDROID_PACKAGE_ID'
                      //     : 'YOUR_IOS_APP_ID';
                      final url = Uri.parse(
                          // Platform.isAndroid
                          "https://play.google.com/store/apps/details?id=com.permata.mahanaim_app"
                          // : "https://apps.apple.com/app/id$appId",
                          );
                      launchUrl(
                        url,
                        mode: LaunchMode.externalApplication,
                      );
                    }
                  },
                  child: const Text("Rate Aplikasi")),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: const Text("Tidak")),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: const Text("Ya")),
            ],
          );
        });

    return exitApp ?? false;
  }
}
