import 'package:get/get.dart';
import 'package:mahanaim_app/model/news_model.dart';
import 'package:mahanaim_app/services/api_services.dart';

class PostDetailsController extends GetxController {
  var isLoading = true.obs;
  var postModel = NewsModel().obs;

  Future<void> fetchPostDetail(
    int postId,
  ) async {
    try {
      isLoading(true);

      var postDetail = await APIService.fetchPostDetails(postId);

      postModel.value = postDetail;
    } finally {
      isLoading(false);
    }
  }
}
