import 'package:get/get.dart';
import 'package:mahanaim_app/model/category_model.dart';
import 'package:mahanaim_app/services/api_services.dart';

class CategoriesController extends GetxController {
  var isLoading = true.obs;

  var categoriesList = List<CategoryModel>.empty().obs;

  @override
  void onInit() {
    fetchCategories();
    super.onInit();
  }

  Future<void> fetchCategories() async {
    try {
      isLoading(true);
      var categories = await APIService.fetchCategories();
      if (categories.length > 0) {
        categoriesList.clear();
        categoriesList.addAll(categories);
      }
    } finally {
      isLoading(false);
    }
  }
}
