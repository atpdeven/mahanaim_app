import 'package:get/get.dart';
import 'package:mahanaim_app/model/news_model.dart';
import 'package:mahanaim_app/services/api_services.dart';

class PostController extends GetxController {
  var isLoading = true.obs;
  var postsList = List<NewsModel>.empty().obs;
  var currentCategoryId = 1;

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> fetchPosts({
    int categoryId = 1,
    int pageNumber = 0,
    int totalRecords = 0,
  }) async {
    try {
      // postsList.clear();
      if (currentCategoryId != categoryId) {
        isLoading(true);
        postsList.clear();
        currentCategoryId = categoryId; // Update the current category ID
      }
      print("total Records" + totalRecords.toString());

      if (postsList.length == 0 || pageNumber == 0) {
        isLoading(true);
        postsList.clear();
      }

      if (postsList.length < totalRecords) {
        var posts = await APIService.fetchPosts(pageNumber, categoryId);
        if (posts.length > 0) {
          postsList.addAll(posts);
        }
      }

      print(postsList.length);
    } finally {
      isLoading(false);
    }
  }
}
