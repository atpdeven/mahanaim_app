import 'package:flutter/material.dart';
import 'package:mahanaim_app/navigation/favorit.dart';
import 'package:mahanaim_app/services/api_services.dart';

class FavoritController extends State<Favorit> {
  static late FavoritController instance;
  late Favorit view;

  @override
  void initState() {
    instance = this;
    getFavoritProducts();
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);

  List products = [];

  getFavoritProducts() async {
    products = await APIService().getFavoritProductList();
    setState(() {});
  }

  addToFavorite(Map item) async {
    await APIService().addToFavorite(item);
    const snackbar = SnackBar(content: Text("Berhasil Favorit"));
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }
}
