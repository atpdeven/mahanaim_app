import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
// import 'package:geocoding/geocoding.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';

class LocationPage extends StatefulWidget {
  const LocationPage({Key? key}) : super(key: key);

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class IntentUtils {
  IntentUtils._();
  static Future<void> launchGoogleMaps() async {
    const double destinationLatitude = -6.906771;
    const double destinationLongitude = 107.620097;
    final uri = Uri(
        scheme: "google.navigation",
        // host: '"0,0"',  {here we can put host}
        queryParameters: {'q': '$destinationLatitude, $destinationLongitude'});
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      debugPrint('An error occurred');
    }
  }

  static Future<void> launchTempatPA(
      double destinationLatitude, double destinationLongitude) async {
    final uri = Uri(
        scheme: "google.navigation",
        queryParameters: {'q': '$destinationLatitude, $destinationLongitude'});
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      debugPrint('An error occurred');
    }
  }
}

class _LocationPageState extends State<LocationPage> {
  double destinationLatitude = 0.0;
  double destinationLongitude = 0.0;
  bool _isLoggedIn = false;

  final TextEditingController latitudeController = TextEditingController();
  final TextEditingController longitudeController = TextEditingController();

  @override
  void dispose() {
    latitudeController.dispose();
    longitudeController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.authStateChanges().listen(
      (user) {
        if (user != null) {
          setState(() {
            _isLoggedIn = true;
          });
        } else {
          setState(() {
            _isLoggedIn = false;
          });
        }
      },
    );
    fetchDestinationCoordinates();
  }

  void fetchDestinationCoordinates() async {
    FirebaseFirestore.instance
        .collection('tempat_pa') // Replace with your collection name
        .doc('GbJRvqMp6Q5bApcSXOd6') // Replace with your document ID
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        setState(() {
          // Update state with retrieved coordinates
          destinationLatitude =
              double.parse(documentSnapshot['destinationLatitude']);
          destinationLongitude =
              double.parse(documentSnapshot['destinationLongitude']);

          print("Latitude : ${destinationLatitude}");
          print(destinationLongitude);
        });
      } else {
        print('Document does not exist on the database');
      }
    }).catchError((error) {
      print('Error fetching document: $error');
    });
  }

  void _updateFirebaseValues() {
    // Update Firebase values with the values entered in the text fields
    FirebaseFirestore.instance
        .collection('tempat_pa')
        .doc('GbJRvqMp6Q5bApcSXOd6')
        .update({
      'destinationLatitude': latitudeController.text,
      'destinationLongitude': longitudeController.text,
    }).then((_) {
      // Show a success message or perform any other action upon successful update
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Firebase values updated successfully'),
      ));
    }).catchError((error) {
      // Handle any errors that occur during the update process
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Failed to update Firebase values'),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Menuju GBKP Bandung Pusat")),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 32),
                  ElevatedButton(
                    onPressed: IntentUtils.launchGoogleMaps,
                    child: const Text(
                      "Rute Menuju Gereja",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                  const SizedBox(height: 32),
                  ElevatedButton(
                    onPressed: () async {
                      await IntentUtils.launchTempatPA(
                          destinationLatitude, destinationLongitude);
                    },
                    child: const Text(
                      "Rute Menuju Tempat PA",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                  const SizedBox(height: 32),
                  // Text field for entering destination latitude
                  if (_isLoggedIn)
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(15),
                          child: Column(
                            children: [
                              TextFormField(
                                controller: latitudeController,
                                decoration: InputDecoration(
                                  labelText: 'Destination Latitude',
                                  border: OutlineInputBorder(),
                                ),
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                              ),
                              const SizedBox(height: 16),
                              // Text field for entering destination longitude
                              TextFormField(
                                controller: longitudeController,
                                decoration: InputDecoration(
                                  labelText: 'Destination Longitude',
                                  border: OutlineInputBorder(),
                                ),
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                              ),
                              const SizedBox(height: 16),
                              // Submit button
                              ElevatedButton(
                                onPressed: _updateFirebaseValues,
                                child: const Text(
                                  "Ganti Alamat PA",
                                  style: TextStyle(color: Colors.white),
                                ),
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.orange,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
