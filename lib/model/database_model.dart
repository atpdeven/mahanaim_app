class DatabaseModel {
  String name;
  String bere_bere;
  String tanggalLahir;

  DatabaseModel({this.name = '', this.bere_bere = '', this.tanggalLahir = ''});

  factory DatabaseModel.fromJson(dynamic json) {
    return DatabaseModel(
      name: "${json['name']}",
      bere_bere: "${json['bere_bere']}",
      tanggalLahir: "${json['tanggalLahir']}",
    );
  }

  Map toJson() => {
    "name":name,
    "bere_bere":bere_bere,
    "tanggalLahir":tanggalLahir
  };
}
