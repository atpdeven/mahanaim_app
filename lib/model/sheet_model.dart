class SheetsColumn{
  static final nama = "Nama";
  static final permata = 'Permata';
  static final belanja ='Belanja';
  static final total ="Total Belanja";
  static final lunas ="Lunas";
  static final waktuPesan ="Waktu Pemesanan";

  static List<String> getColumns() =>[nama, permata,belanja,total,lunas,waktuPesan];
}