import 'dart:convert';

import 'package:intl/intl.dart';

List<NewsModel> postsFromJson(String str) =>
    List<NewsModel>.from(json.decode(str).map((x) => NewsModel.fromJson(x)));

NewsModel postDetailsFromJson(String str) =>
    NewsModel.fromJson(json.decode(str));

class NewsModel {
  late int? id;
  late String? title;
  late String? imageURL;
  late String? postedDate;
  late int? categoryId;
  late String? categoryName;
  late String? postContent;

  NewsModel({
    this.id,
    this.title,
    this.imageURL,
    this.postedDate,
    this.categoryId,
    this.categoryName,
    this.postContent,
  });

  NewsModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    title = json["title"];
    imageURL = json["image_url"];
    postedDate = json["post_date"] != null
        ? DateFormat("dd-MMM-yyyy").format(DateTime.parse(json["post_date"]))
        : null;
    categoryName = json["category_name"];
    postContent = json["post_content"];
  }

  Map<String, dynamic> newsModelToMap(NewsModel newsModel) {
  return {
    'id': newsModel.id,
    'title': newsModel.title,
    'imageURL': newsModel.imageURL,
    'categoryName': newsModel.categoryName,
    'postedDate': newsModel.postedDate,
  };
}
}