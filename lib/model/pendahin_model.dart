import 'package:cloud_firestore/cloud_firestore.dart';

class Pendahin {
  final String image;
  final String title;
  final String subtitle;
  final String rating;
  final String instagram;
  final String instagramLink;
  final String tingkat;

  Pendahin(
      {required this.image,
      required this.title,
      required this.subtitle,
      required this.rating,
      required this.instagram,
      required this.tingkat,
      required this.instagramLink});

  factory Pendahin.fromJson(DocumentSnapshot doc) {
    Map data = doc.data() as Map<String, dynamic>;
    return Pendahin(
      title: data['title'],
      image: data['image'],
      subtitle: data['subtitle'],
      rating: data['rating'],
      instagram: data['instagram'],
      instagramLink: data['instagramLink'],
      tingkat: data['tingkat'],
    );
  }
}
