class KEE {
  String id;
  final String nomor;
  final String title;
  final String lyrics;
  final String youtubeLink;

  KEE(
      {this.id = '',
      required this.title,
      required this.lyrics,
      required this.nomor,
      required this.youtubeLink});

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'nomor': nomor,
        'lyrics': lyrics,
        'youtubeLink': youtubeLink,
      };

  static KEE fromJson(Map<String, dynamic> json) => KEE(
        id: json['id'],
        title: json['title'],
        lyrics: json['lyrics'],
        nomor: json['nomor'],
        youtubeLink: json['youtubeLink']
      );
}
