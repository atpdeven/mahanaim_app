class CardItem {
  final String title;
  final String pricing;
  final String desc;
  final List<String> images;
  int currentIndex;

  CardItem({
    required this.title,
    required this.pricing,
    required this.images,
    required this.desc,
    this.currentIndex = 0,
  });
}
