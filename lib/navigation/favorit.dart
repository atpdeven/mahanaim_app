import 'package:flutter/material.dart';
import 'package:mahanaim_app/controllers/favorite_controller.dart';

class Favorit extends StatefulWidget {
  const Favorit({Key? key}) : super(key: key);
  Widget build(context, FavoritController controller) {
    controller.view = this;
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorit"),
      ),
      body: GridView.builder(
        itemCount: controller.products.length,
        shrinkWrap: true,
        physics: ScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 8,
          mainAxisSpacing: 8,
        ),
        itemBuilder: (BuildContext context, int index) {
          var item = controller.products[index];
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(
                            item["image_url"],
                          ),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(6.0))),
                    child: Stack(
                      children: [
                        Positioned(
                          right: 6.0,
                          top: 8.0,
                          child: CircleAvatar(
                            radius: 14.0,
                            backgroundColor: Colors.white,
                            child: Icon(
                              Icons.favorite,
                              color: Colors.red,
                              size: 14.0,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  item["title"],
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 4.0,
                ),
                Text(
                  item["posted_date"],
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  @override
  State<Favorit> createState() => FavoritController();
}
