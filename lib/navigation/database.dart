import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DatabasePermata extends StatefulWidget {
  const DatabasePermata({super.key});

  @override
  State<DatabasePermata> createState() => _DatabasePermataState();
}

class _DatabasePermataState extends State<DatabasePermata> {
  final controller = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..loadRequest(Uri.parse(
        "https://docs.google.com/forms/d/e/1FAIpQLSc1kS2No0c6o4DaKnkLR0WA1yXdCsSGjRxmI7jZq9ePcZhGWw/viewform"))
    ..setNavigationDelegate(
      NavigationDelegate(
          onNavigationRequest: (NavigationRequest request) async {
        if (request.url.startsWith('https://chat.whatsapp.com/')) {
          debugPrint('blocking navigation to ${request.url}');
          await _launchURL("https://chat.whatsapp.com/I3FMhl8W6LH4csOXGvp5qp");
          return NavigationDecision.prevent;
          // return NavigationDecision.prevent;
        }
        return NavigationDecision.navigate;
      }),
    );
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Database Mahanaim"),
      ),
      body: WebViewWidget(controller: controller),
    );
  }
}

_launchURL(String url) async {
  if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
}
