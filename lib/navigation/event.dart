import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mahanaim_app/component/add_event.dart';
// import 'package:mahanaim_app/component/edit_event.dart';
import 'package:mahanaim_app/model/event_model.dart';
// import 'package:get/get.dart';

class EventScreen extends StatefulWidget {
  const EventScreen({super.key});

  @override
  State<EventScreen> createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  DateTime? _selectedDate;
  bool _isLoggedIn = false;
  final _random = Random();

  Map<String, List<String>> eventImages = {
    'PA Sektor': [
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PaSektor_5.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PaSektor_2.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PASektor_1.jpg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PaSektor_4.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PaSektor_6.jpg"
    ],
    'PA Gabungan': [
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PAGabungan_7.jpg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PAGabungan_1.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PAGabungan_2.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PAGabungan_5.jpg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PaSektor_3.jpg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/PAGabungan_6.jpg",
    ],
    'Latihan VG': [
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/WhatsApp-Image-2024-02-22-at-2.55.47-PM.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/WhatsApp-Image-2024-02-22-at-2.55.48-PM.jpeg",
    ],
    'Other': ["https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/filefix.jpg"],
    'Acara Kebersamaan': [
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/AcaraKebersamaan_1.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/AcaraKebersamaan_2.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/AcaraKebersamaan_3.jpeg"
    ],
    'Kunjungan Kasih': [
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/KunjunganKasih_1.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/Kunjungan_Kasih2.jpeg",
    ],
    'Wisuda': [
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/Wisuda_1.jpeg",
      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/Wisuda_2.jpeg",
    ],
  };

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.authStateChanges().listen(
      (user) {
        if (user != null) {
          setState(() {
            _isLoggedIn = true;
          });
        } else {
          setState(() {
            _isLoggedIn = false;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.blue[200],
        title: Row(
          children: [
            Text(_selectedDate != null
                ? _selectedDate!.toIso8601String().substring(0, 10)
                : "All Events"),
            if (_selectedDate != null)
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  setState(() {
                    _selectedDate = null;
                  });
                },
              )
          ],
        ),
        actions: [
          IconButton(
            onPressed: () async {
              DateTime? newDate = await showDatePicker(
                context: context,
                initialDate: _selectedDate ?? DateTime.now(),
                firstDate: DateTime(2020),
                lastDate: DateTime(2030),
              );
              if (newDate != null) {
                setState(() {
                  _selectedDate = newDate;
                });
              }
            },
            icon: Icon(Icons.calendar_today),
          ),
          if (_isLoggedIn) // Show the button if logged in
            IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddEventScreen()),
                );
              },
              icon: Icon(Icons.add),
            )
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _selectedDate == null
            ? FirebaseFirestore.instance.collection('events').snapshots()
            : FirebaseFirestore.instance
                .collection('events')
                .where('date', isEqualTo: _selectedDate)
                .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text("Terdapat kesalahan: ${snapshot.error}"),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          List<Event> events =
              snapshot.data!.docs.map((e) => Event.fromJson(e)).toList();
          return GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                crossAxisSpacing: 8,
                mainAxisSpacing: 0,
                childAspectRatio: 3 / 2),
            itemCount: events.length,
            itemBuilder: (context, index) {
              events.sort((a, b) => a.date.compareTo(b.date));
              Event event = events[index];
              final formattedDate =
                  DateFormat('dd MMM yyyy').format(event.date);
              final daysUntilEvent =
                  event.date.difference(DateTime.now()).inDays;
              final daysText = daysUntilEvent == -1
                  ? 'Hari Ini!'
                  : '${daysUntilEvent + 1} hari lagi';
              List<String>? imageList = eventImages[event.title];
              String imageUrl = imageList != null && imageList.isNotEmpty
                  ? imageList[_random.nextInt(imageList.length)]
                  : eventImages['Other']!.first;
              return Container(
                height: double.infinity,
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          child: ClipRRect(
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(16.0),
                                topRight: Radius.circular(16.0)),
                            child: Align(
                              alignment: Alignment(-0.5, -0.2),
                              widthFactor: 1,
                              heightFactor: 0.8,
                              child: Image.network(
                                imageUrl,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        if (_isLoggedIn)
                          Align(
                            alignment: Alignment.bottomRight,
                            child: IconButton(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text("Konfirmasi"),
                                      content: Text(
                                          "Apakah Anda yakin ingin menghapus acara ini?"),
                                      actions: [
                                        TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text("Tidak"),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            FirebaseFirestore.instance
                                                .collection('events')
                                                .doc(event.id)
                                                .delete();
                                            setState(() {
                                              events.removeAt(index);
                                            });
                                            Navigator.of(context).pop();
                                          },
                                          child: Text("Ya"),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                              icon: Icon(
                                Icons.edit,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                      ],
                    ),
                    Container(
                      child: Center(
                        child: ListTile(
                          title: Text(event.title),
                          subtitle: Row(
                            children: [
                              SizedBox(width: 8),
                              Text(
                                daysText,
                                style: TextStyle(
                                  color: daysUntilEvent == 0
                                      ? Colors.black // Event is today
                                      : daysUntilEvent < 0
                                          ? Colors.red // Event has passed
                                          : Colors
                                              .green, // Event is in the future
                                ),
                              ),
                            ],
                          ),
                          trailing: Text(
                            formattedDate,
                            style: TextStyle(fontSize: 25),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
