import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mahanaim_app/component/product_details.dart';
import 'package:mahanaim_app/model/product_model.dart';

class Lapak_Permata extends StatefulWidget {
  const Lapak_Permata({super.key});

  @override
  State<Lapak_Permata> createState() => _Lapak_PermataState();
}

class _Lapak_PermataState extends State<Lapak_Permata> {
  List<CardItem> cardItems = [];

  String searchText = "";
  @override
  void initState() {
    super.initState();
    fetchCardItems();
  }

  void fetchCardItems() async {
    QuerySnapshot querySnapshot =
        await FirebaseFirestore.instance.collection('product').get();

    setState(() {
      cardItems = querySnapshot.docs.map((doc) {
        Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
        return CardItem(
          title: data['name'] ?? '',
          desc: data['desc'] ?? '',
          pricing: data['price'] != null ? data['price'].toString() : '',
          images: List<String>.from(data['image'] ?? []),
          currentIndex: 0,
        );
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus &&
                    currentFocus.focusedChild != null) {
                  currentFocus.focusedChild?.unfocus();
                }
              },
              child: Container(
                height: 80,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(color: Colors.blue[200]),
                child: Container(
                  color: Colors.white,
                  margin: EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                            hintText: "Cari Produk Permata",
                            hintStyle:
                                TextStyle(fontSize: 15, color: Colors.grey),
                            border: InputBorder.none,
                            icon: Icon(Icons.search),
                          ),
                          onChanged: (value) {
                            setState(() {
                              searchText = value;
                            });
                          },
                        ),
                      ),
                      IconButton(
                          onPressed: () {}, icon: Icon(Icons.filter_list))
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
                child: GridView.count(
                    crossAxisCount: 2,
                    // physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: cardItems
                        .where((cardItem) => cardItem.title
                            .toLowerCase()
                            .contains(searchText.toLowerCase()))
                        .map((cardItem) {
                      return buildCard(cardItem);
                    }).toList()))
          ],
        ),
      ),
    );
  }

  Widget buildCard(CardItem cardItem) {
    return GestureDetector(
      onTap: () async {
        await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductDetail(
                      cardItem: cardItem,
                    )));
      },
      child: Card(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height / 8,
                child: LayoutBuilder(builder: (context, constraints) {
                  return PageView.builder(
                    itemCount: cardItem.images.length,
                    onPageChanged: (int index) {
                      setState(() {
                        cardItem.currentIndex = index;
                      });
                    },
                    itemBuilder: (context, index) {
                      return ConstrainedBox(
                        constraints: BoxConstraints.expand(
                            height: constraints.maxHeight),
                        child: Image.network(
                          cardItem.images[index],
                          fit: BoxFit.cover,
                        ),
                      );
                    },
                  );
                }),
              ),
              Row(
                children: List<Widget>.generate(
                  cardItem.images.length,
                  (int circleIndex) {
                    return Padding(
                      padding: EdgeInsets.all(4),
                      child: CircleAvatar(
                        radius: 4,
                        backgroundColor: circleIndex == cardItem.currentIndex
                            ? Colors.blue
                            : Colors.grey,
                      ),
                    );
                  },
                ),
              ),
              ListTile(
                title: Text(
                  cardItem.title,
                  style: TextStyle(color: Colors.black),
                ),
                subtitle: Text("Rp ${cardItem.pricing}"),
                trailing: Container(
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    " Premium ",
                    style: TextStyle(color: Colors.white, fontSize: 10),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
