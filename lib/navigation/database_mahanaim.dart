import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mahanaim_app/model/database_model.dart';
import 'dart:convert' as convert;

class DatabaseMahanaim extends StatefulWidget {
  const DatabaseMahanaim({Key? key}) : super(key: key);

  @override
  State<DatabaseMahanaim> createState() => _DatabaseMahanaimState();
}

class _DatabaseMahanaimState extends State<DatabaseMahanaim> {
  List<DatabaseModel> database = [];
  List<DatabaseModel> filteredDatabase = [];
  bool isLoading = true; // Track loading state
  TextEditingController searchController = TextEditingController();

  getFeedbackFromSheet() async {
    var raw = await http.get(
      Uri.parse(
          "https://script.googleusercontent.com/macros/echo?user_content_key=5xiqMrP04FH8hcb447GQ54IUqxCllQ59u-aJznNbudMABquTblAudQ-Rk7_FC_WO6BY0Qs11muZxE4AHA-4E2B9XLGMEryhRm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnEZWRFDKjZAylyNnyo7xr75z9XXlgm-7zK3hHFp9MNSkFpLOtH-xk2tJ7LbPDXg7X5dy2--EK4bGr_ZGiXKJIPCEdpUuSjPpstz9Jw9Md8uu&lib=MdVR1CNtUgsbmrAo9zxP-eYZZ450w33tT"),
    );

    var jsonDatabase = convert.jsonDecode(raw.body);

    jsonDatabase.forEach((element) {
      DatabaseModel databaseModel = new DatabaseModel();
      databaseModel.name = element['nama'];
      databaseModel.bere_bere = element['bere-bere'];
      databaseModel.tanggalLahir = element['tanggalLahir'];

      database.add(databaseModel);
    });

    setState(() {
      isLoading = false; // Update loading state
      filteredDatabase =
          List.from(database); // Copy database to filteredDatabase initially
    });
  }

  @override
  void initState() {
    getFeedbackFromSheet();
    super.initState();
  }

  void filterSearchResults(String query) {
    List<DatabaseModel> dummySearchList = [];
    dummySearchList.addAll(database);
    if (query.isNotEmpty) {
      List<DatabaseModel> dummyListData = [];
      dummySearchList.forEach((item) {
        if (item.name.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        filteredDatabase.clear();
        filteredDatabase.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        filteredDatabase.clear();
        filteredDatabase.addAll(database);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Database Mahanaim (${filteredDatabase.length})"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.clear),
            onPressed: () {
              searchController.clear();
              filterSearchResults('');
            },
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(56),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: searchController,
              onChanged: (value) {
                filterSearchResults(value);
              },
              decoration: InputDecoration(
                hintText: 'Cari Berdasarkan Nama',
              ),
            ),
          ),
        ),
      ),
      body: isLoading
          ? Center(
              child:
                  CircularProgressIndicator(), // Show circular progress indicator while loading
            )
          : Container(
              child: ListView.builder(
                itemCount: filteredDatabase.length,
                itemBuilder: ((context, index) {
                  return DatabaseTile(
                    name: filteredDatabase[index].name,
                    bere_bere: filteredDatabase[index].bere_bere,
                    tanggalLahir: filteredDatabase[index].tanggalLahir,
                  );
                }),
              ),
            ),
    );
  }
}

class DatabaseTile extends StatelessWidget {
  final String name, bere_bere, tanggalLahir;

  const DatabaseTile(
      {Key? key,
      required this.name,
      required this.bere_bere,
      required this.tanggalLahir})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: Text(name),
                ),
                Text(tanggalLahir)
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(bere_bere),
            ),
          ),
          Divider()
        ],
      ),
    );
  }
}
