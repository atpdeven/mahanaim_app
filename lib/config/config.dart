class Config{
  static String apiURL = "https://bangunsabahkendit.my.id/wp-json/wp/v2/";
  static String categoryURL = "categories";
  static String postsURL = "latest-posts/?page_size=10&category_id=";
  static String postDetailURL = "post-details/?id=";
  static String dateFormat = "dd-MMM-yyyy";
}