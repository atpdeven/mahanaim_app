import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mahanaim_app/model/news_model.dart';
import 'package:mahanaim_app/widget/news_details.dart';
import 'package:mahanaim_app/utilities/layout_utility.dart';

class NewsCardWidget extends StatelessWidget {
  // const NewsCardWidget({super.key});
  final NewsModel model;

  NewsCardWidget({required this.model});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: InkWell(
        onTap: () {
          Get.to(NewsDetails(), arguments: model.id);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: [
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0)),
                  child: Align(
                        alignment: Alignment(-0.5, -0.2),
                        // widthFactor: 1,
                        heightFactor: 0.5,
                        child: Image.network(model.imageURL ??
                            "https://i.ibb.co/pPRVQy7/file.jpg"),
                      ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(15)),
                    child: Text(
                      model.categoryName ?? "Mahanaim",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    model.title ?? "Mahanaim",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      LayoutUtils.iconText(
                          Icon(Icons.timer),
                          Text(
                            model.postedDate ?? "Mahanaim",
                            style: TextStyle(fontSize: 14, color: Colors.black),
                          )),
                      GestureDetector(
                        onTap: () {},
                        child: const Icon(Icons.favorite_border),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
