// import 'package:flutter/foundation.dart';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:mahanaim_app/component/auth_page.dart';
import 'package:mahanaim_app/navigation/database_mahanaim.dart';
import 'package:mahanaim_app/pages/Alkitab_Indonesia.dart';
import 'package:mahanaim_app/pages/Alkitab_Karo.dart';
import 'package:mahanaim_app/pages/about_us.dart';
import 'package:mahanaim_app/pages/daftar_kee.dart';
import 'package:mahanaim_app/pages/notification.dart';
import 'package:mahanaim_app/pages/pendahin_Permata.dart';
import 'package:mahanaim_app/pages/profile.dart';
// import 'package:mahanaim_app/pages/tambah_kee.dart';
import 'package:mahanaim_app/theme/theme_provider.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:flutter/widgets.dart';

class NavBar extends StatelessWidget {
  final bool isLoggedIn; // Variable to track login status

  const NavBar({Key? key, required this.isLoggedIn}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(
              "Mahanaim",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            accountEmail: Text(
              "Mahanaim_banpus@gmail.com",
              style: TextStyle(color: Colors.white),
            ),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network("https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/profile.png"),
              ),
            ),
            decoration: BoxDecoration(
                color: Colors.grey,
                image: DecorationImage(
                    image: AssetImage('image/mahanaim1.jpg'),
                    fit: BoxFit.fill)),
          ),
          ListTile(
            leading: Icon(Icons.book_online),
            title: Text("Alkitab Bahasa Indonesia"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const AlkitabIndonesia()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.book_online_outlined),
            title: Text("Alkitab Bahasa Karo"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const AlkitabKaro()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.note),
            title: Text("KEE (Kitab Ende Enden)"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const DaftarKEE()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.bookmark),
            title: Text("Bookmark*"),
            onTap: () => print("Profile"),
          ),
          ListTile(
            leading: Icon(Icons.star),
            title: Text("Beri Peringkat"),
            onTap: () {
              if (Platform.isAndroid || Platform.isIOS) {
                // final appId = Platform.isAndroid
                //     ? 'YOUR_ANDROID_PACKAGE_ID'
                //     : 'YOUR_IOS_APP_ID';
                final url = Uri.parse(
                    // Platform.isAndroid
                    "https://play.google.com/store/apps/details?id=com.permata.mahanaim_app"
                    // : "https://apps.apple.com/app/id$appId",
                    );
                launchUrl(
                  url,
                  mode: LaunchMode.externalApplication,
                );
              }
            },
          ),
          ListTile(
            leading: Icon(Icons.share),
            title: Text("Share"),
            onTap: () => shareApp(),
          ),
          ListTile(
            leading: Icon(Icons.notification_important),
            title: Text("Izin Notifikasi"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const Notification_Checker()),
              );
            },
          ),
          ListTile(
              leading: Icon(Icons.view_agenda),
              title: Text("Ubah Thema"),
              onTap: () {
                Provider.of<ThemeProvider>(context, listen: false)
                    .toggleTheme();
              }),
          ListTile(
            leading: Icon(Icons.workspace_premium),
            title: Text("Pendahin Permata"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PendahinPermata()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text("Tentang Aplikasi"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AboutUs()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.chat),
            title: Text("Tentang Developer"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfileScreen()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.admin_panel_settings),
            title: Text("Login Admin"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AuthPage()),
              );
            },
          ),
          if (isLoggedIn) // Conditionally show based on login status
            ListTile(
              leading: Icon(Icons.admin_panel_settings),
              title: Text("Database Mahanaim"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DatabaseMahanaim()),
                );
              },
            ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              "*Masih dalam Tahap Pengembangan",
              style: (TextStyle(fontSize: 10, color: Colors.red)),
            ),
          )
        ],
      ),
    );
  }

  Future<void> shareApp() async {
    // Set the app link and the message to be shared
    final String appLink =
        'https://play.google.com/store/apps/details?id=com.permata.mahanaim_app&hl=en&gl=US';
    final String message = 'Install Mahanaim_app disini: $appLink';

    // Share the app link and message using the share dialog
    await FlutterShare.share(
        title: 'Share App', text: message, linkUrl: appLink);
  }
}
