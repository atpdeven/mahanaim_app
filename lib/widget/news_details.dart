import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_icons_null_safety/flutter_icons_null_safety.dart';
import 'package:get/get.dart';
import 'package:mahanaim_app/controllers/post_detail.controller.dart';
import 'package:mahanaim_app/utilities/layout_utility.dart';
import 'package:widget_zoom/widget_zoom.dart';

class NewsDetails extends StatelessWidget {
  NewsDetails({super.key});

  final PostDetailsController newsDetailController =
      Get.put(PostDetailsController());

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero, () async {
      await newsDetailController.fetchPostDetail(Get.arguments);
    });
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            // flexibleSpace: FlexibleSpaceBar(
            //   background: Image.network(
            //     "https://i.ibb.co/pPRVQy7/file.jpg",
            //     fit: BoxFit.cover,
            //   ),
            // ),
            flexibleSpace: FlexibleSpaceBar(background: backgroundImage()),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            expandedHeight: 250,
          ),
          SliverFillRemaining(child: Obx(() {
            if (newsDetailController.isLoading.value) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return newsDetailSection();
            }
          }))
        ],
      ),
    );
  }

  Widget backgroundImage() {
    return Obx(() {
      if (newsDetailController.isLoading.value) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return Center(
            child: WidgetZoom(
          heroAnimationTag: 'tag',
          zoomWidget: Image.network(
            newsDetailController.postModel.value.imageURL ??
                "https://i.ibb.co/pPRVQy7/file.jpg",
            fit: BoxFit.cover,
          ),
        ));
      }
    });
  }

  Widget newsDetailSection() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          row1(),
          LayoutUtils.iconText(
              Icon(Icons.timer),
              Text(newsDetailController.postModel.value.postedDate ??
                  "Mahanaim")),
          const SizedBox(
            height: 15,
          ),
          Text(
            newsDetailController.postModel.value.title ?? "Mahanaim",
            style: TextStyle(
                fontSize: 16, color: Colors.grey, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            width: 100,
            child: Divider(
              color: Colors.grey,
              thickness: 2,
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              physics: ClampingScrollPhysics(),
              child: Html(
                style: {
                  "p": Style(
                    color: Colors.black,
                    fontSize: FontSize.larger,
                  ),
                  "h1": Style(color: Colors.black, fontSize: FontSize.xLarge),
                  // Add more styles as needed
                },
                data: newsDetailController.postModel.value.postContent ?? '',
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget row1() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.green, borderRadius: BorderRadius.circular(10)),
            child: Text(
              newsDetailController.postModel.value.categoryName ?? "Mahanaim",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        const Align(
          alignment: Alignment.topRight,
          child: Row(
            children: [
              IconButton(
                onPressed: null,
                icon: Icon(Feather.moon),
              ),
              IconButton(onPressed: null, icon: Icon(Feather.bookmark))
            ],
          ),
        )
      ],
    );
  }
}
