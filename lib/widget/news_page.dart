import 'package:flutter/material.dart';
import 'package:mahanaim_app/controllers/posts_controller.dart';
import 'package:get/get.dart';
import 'package:mahanaim_app/widget/news_card_widget.dart';

class NewsPage extends StatefulWidget {
  // const NewsPage({super.key, int? categoryId});

  final int categoryID;
  final bool isReload;
  final int totalRecords;

  NewsPage(
      {required this.categoryID,
      required this.isReload,
      required this.totalRecords});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  final PostController postController = Get.put(PostController());

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  ScrollController _scrollController = new ScrollController();
  int _page = 1;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async {
      if (this.widget.isReload) {
        await postController.fetchPosts(
          categoryId: this.widget.categoryID,
          pageNumber: 1,
          totalRecords: this.widget.totalRecords,
        );
      }
    });

    _scrollController.addListener(() async {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        await postController.fetchPosts(
          pageNumber: ++_page,
          totalRecords: this.widget.totalRecords,
          categoryId: this.widget.categoryID,
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return newsList();
  }

  Widget newsList() {
    return Container(
      child: Obx(() {
        if (postController.isLoading.value)
          return Center(
            child: CircularProgressIndicator(),
          );
        else
          return RefreshIndicator(
              key: refreshKey,
              // onRefresh: ()=>postController.fetchPosts(categoryId: this.widget.categoryID),
              onRefresh: () async {
                this._page = 1;
                await postController.fetchPosts(
                    pageNumber: 1,
                    totalRecords: this.widget.totalRecords,
                    categoryId: this.widget.categoryID);
              },
              child: ListView.builder(
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: postController.postsList.length,
                controller: _scrollController,
                itemBuilder: (context, index) {
                  if((index == postController.postsList.length - 1) && postController.postsList.length < this.widget.totalRecords){
                    return(Center(child: CircularProgressIndicator(),));
                  }
                  return NewsCardWidget(model: postController.postsList[index]);
                },
              ));
      }),
    );
  }
}
