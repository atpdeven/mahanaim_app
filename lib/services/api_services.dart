import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mahanaim_app/config/config.dart';
import 'package:mahanaim_app/model/category_model.dart';
import 'package:mahanaim_app/model/news_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class APIService {
  static var client = http.Client();

  static Future<List<CategoryModel>> fetchCategories() async {
    var response =
        await client.get(Uri.parse(Config.apiURL + Config.categoryURL));

    if (response.statusCode == 200) {
      var jsonString = response.body;
      return categoryFromJson(jsonString);
    } else {
      throw Exception('Failed to load data');
    }
  }

  static Future<List<NewsModel>> fetchPosts(
    int pageNumber,
    int categoryId,
  ) async {
    var url = Config.apiURL +
        Config.postsURL +
        categoryId.toString(); 
        // +
        // "&page_no=" +
        // pageNumber.toString();
    var response = await client.get(Uri.parse(url));

    print(url);

    if (response.statusCode == 200) {
      var jsonString = response.body;
      return postsFromJson(jsonString);
    } else {
      throw Exception('Failed to load data');
    }
  }

  static Future<NewsModel> fetchPostDetails(
    int postId,
  ) async {
    var response = await client.get(
      Uri.parse(Config.apiURL + Config.postDetailURL + postId.toString()),
    );

    if (response.statusCode == 200) {
      var jsonString = response.body;

      return postDetailsFromJson(jsonString);
    } else {
      throw Exception('Failed to load data');
    }
  }

  addToFavorite(Map item) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List favoritList = jsonDecode((prefs.getString("favorit_list") ?? "[]"));
    favoritList.add(item);
    prefs.setString("favorit_list", jsonEncode(favoritList));
  }

  getFavoritProductList()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List favoritList = jsonDecode((prefs.getString("favorit_list") ?? "[]"));
    return favoritList;
  }
}
