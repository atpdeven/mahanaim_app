import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:mahanaim_app/provider/cart_provide.dart';
import 'package:mahanaim_app/component/googlesheets.dart';
import 'package:mahanaim_app/firebase_options.dart';
import 'package:mahanaim_app/home_page.dart';
// import 'package:mahanaim_app/theme/theme.dart';
import 'package:mahanaim_app/theme/theme_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'provider/globals.dart' as globals;
import 'package:provider/provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  ByteData data = await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
  SecurityContext.defaultContext.setTrustedCertificatesBytes(data.buffer.asUint8List());
  MobileAds.instance.initialize();
  await Permission.notification.isDenied.then((value) {
    if (value) {
      Permission.notification.request();
    }
  });
  await SheetsFlutter.init();
  // await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  // await initFcm();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  globals.appNavigator = GlobalKey<NavigatorState>();
  // runApp(ChangeNotifierProvider(
  //   create: (context) => ThemeProvider(),
  //   child: const MyApp(),
  // ));
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => ThemeProvider(),
      ),
      ChangeNotifierProvider(
        create: (context) => CartProvider(),
      )
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      // theme: ThemeData(
      //   primaryColor: Colors.white,
      //   useMaterial3: true,
      // ),
      navigatorKey: globals.appNavigator,
      home: HomePage(),
      theme: Provider.of<ThemeProvider>(context).themeData,
      // darkTheme: darkMode,
    );
  }
}
