import 'package:flutter/material.dart';
import 'package:flutter_icons_null_safety/flutter_icons_null_safety.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profile Developer",
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(
                width: 120,
                height: 120,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network(
                      "https://bangunsabahkendit.my.id/wp-content/uploads/2024/02/profile.png"),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Abdi Jepri Bangun",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(
                "abdijepri@gmail.com",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 200,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.yellow,
                    side: BorderSide.none,
                    shape: StadiumBorder(),
                  ),
                  child: Text(
                    "Permata Mahanaim",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Divider(),
              SizedBox(
                height: 10,
              ),
              ProfileList(
                title: "No HP/WA",
                trailing: "081296214780",
                icon: LineAwesomeIcons.whatsapp,
                onPress: () {},
              ),
              ProfileList(
                title: "Instagram",
                trailing: "@abdijepri",
                icon: LineAwesomeIcons.instagram,
                onPress: () {},
              ),
              ProfileList(
                title: "Facebook",
                trailing: "AbdiJepriBangun",
                icon: LineAwesomeIcons.facebook,
                onPress: () {},
              ),
              ProfileList(
                title: "Twitter",
                trailing: "@abdijepri",
                icon: LineAwesomeIcons.twitter,
                onPress: () {},
              ),
              ProfileList(
                title: "GitLab",
                trailing: "@atpdeven",
                icon: LineAwesomeIcons.gitlab,
                onPress: () {},
              ),
              ProfileList(
                title: "Saweria",
                trailing: "@bangunsabahkendit",
                icon: Feather.gift,
                onPress: () {},
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 200,
                child: ElevatedButton(
                  onPressed: () {
                    _launchInBrowser(
                      Uri.parse("https://saweria.co/bangunsabahkendit"),
                    );
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green,
                    side: BorderSide.none,
                    shape: StadiumBorder(),
                  ),
                  child: Text(
                    "Kirimkan Kami Kopi",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw Exception('Could not launch $url');
    }
  }
}

class ProfileList extends StatelessWidget {
  const ProfileList({
    super.key,
    required this.title,
    required this.icon,
    required this.trailing,
    required this.onPress,
    // required this.endIcon,
    // this.texcolor,
  });

  final String title;
  final IconData icon;
  final VoidCallback onPress;
  final String trailing;
  // final bool endIcon;
  // final Color? texcolor;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        width: 35,
        height: 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: Colors.teal.withOpacity(0.1),
        ),
        child: Icon(
          icon,
          color: Colors.black,
        ),
      ),
      title: Text(title),
      trailing: Text(
        trailing,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
