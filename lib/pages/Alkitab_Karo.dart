import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AlkitabKaro extends StatefulWidget {
  const AlkitabKaro({super.key});

  @override
  State<AlkitabKaro> createState() => _AlkitabKaro();
}

class _AlkitabKaro extends State<AlkitabKaro> {

  final controller = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.disabled)
    ..loadRequest(Uri.parse("https://alkitab.mobi/karo/"));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alkitab Bahasa Karo"),
      ),
      body: WillPopScope(
        onWillPop: ()async{
          if(await controller.canGoBack()){
            controller.goBack();
            return false;
          }else{
            return true;
          }
          // return false;
        },
        child: WebViewWidget(controller: controller),
      ),
    );
  }
}
