// import 'dart:io';

import 'package:flutter/material.dart';
// import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
// import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:mahanaim_app/controllers/categories_controller.dart';
// import 'package:mahanaim_app/navbar.dart';
// import 'package:mahanaim_app/navigation/favorit.dart';
import 'package:mahanaim_app/widget/news_page.dart';
import 'package:mahanaim_app/pages/notification_page.dart';
// import 'package:mahanaim_app/utilities/geo_location.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'globals.dart' as globals;
// import 'package:mahanaim_app/widget/news_card_widget.dart';

class Home_Screen extends StatefulWidget {
  const Home_Screen({super.key});

  @override
  State<Home_Screen> createState() => _Home_ScreenState();
}

class _Home_ScreenState extends State<Home_Screen> {
  // List<Widget> tabs = [];
  // String _debugLabelString = "";
  // String? _emailAddress;
  // String? _smsNumber;
  // String? _externalUserId;
  // String? _language;
  // bool _enableConsentButton = false;
  // bool _requireConsent = false;

  // static final String oneSignalAppId = "6f3f1362-57b9-4024-adeb-325ccd4d0471";

  // int _currentIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  final CategoriesController categoriesController =
      Get.put(CategoriesController());

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Obx(() {
        if (categoriesController.isLoading.value)
          return Center(
            child: CircularProgressIndicator(),
          );
        else
          return DefaultTabController(
            // onWillPop : ()=> _onBackButtonPressed(context),
            length: categoriesController.categoriesList.length,
            child: Scaffold(
              
              backgroundColor: Theme.of(context).colorScheme.background,
              appBar: AppBar(
                toolbarHeight: 0,
                bottom: TabBar(
                  tabs: categoriesController.categoriesList
                      .map((model) => tab(model.categoryName))
                      .toList(),
                  isScrollable: true,
                  labelColor: Colors.red,
                  unselectedLabelColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorColor: Colors.white,
                ),
              ),
              body: TabBarView(
                children: categoriesController.categoriesList.map((model) {
                  return NewsPage(
                    categoryID: model.categoryId,
                    isReload: true,
                    totalRecords: model.count,
                  );
                }).toList(),
              ),
              // bottomNavigationBar: Container(
              //   color: Colors.black,
              //   // shape: CircularNotchedRectangle(),
              //   // notchMargin: 5.0,
              //   child: Padding(
              //     padding:
              //         const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              //     child: GNav(
              //       backgroundColor: Colors.black,
              //       color: Colors.white,
              //       activeColor: Colors.white,
              //       tabBackgroundColor: Colors.grey.shade800,
              //       padding: EdgeInsets.all(10),
              //       gap: 8,
              //       tabs: [
              //         GButton(
              //           icon: Icons.home,
              //           text: "Beranda",
              //         ),
              //         GButton(
              //           icon: Icons.favorite,
              //           text: "Favorit",
              //           onPressed: () {
              //             Navigator.push(
              //               context,
              //               MaterialPageRoute(builder: (context) => Favorit()),
              //             );
              //           },
              //         ),
              //         GButton(
              //           icon: Icons.search,
              //           text: "Lapak_Permata",
              //         ),
              //         GButton(
              //           icon: Icons.settings,
              //           text: "Pengaturan",
              //         )
              //       ],
              //     ),
              //   ),
              // ),
            ),
          );
      }),
    );
  }

  

  Widget tab(String tabName) {
    return Tab(text: tabName);
  }

  Future<void> initPlatformState() async {
    if (!mounted) return;

    OneSignal.Debug.setLogLevel(OSLogLevel.verbose);

    OneSignal.Debug.setAlertLevel(OSLogLevel.none);
    // OneSignal.consentRequired(_requireConsent);

    // NOTE: Replace with your own app ID from https://www.onesignal.com
    OneSignal.initialize("6f3f1362-57b9-4024-adeb-325ccd4d0471");

    // AndroidOnly stat only
    // OneSignal.Notifications.removeNotification(1);
    // OneSignal.Notifications.removeGroupedNotifications("group5");

    OneSignal.Notifications.clearAll();

    OneSignal.User.pushSubscription.addObserver((state) {
      print(OneSignal.User.pushSubscription.optedIn);
      print(OneSignal.User.pushSubscription.id);
      print(OneSignal.User.pushSubscription.token);
      print(state.current.jsonRepresentation());
    });

    OneSignal.Notifications.addPermissionObserver((state) {
      print("Has permission " + state.toString());
    });

    OneSignal.Notifications.addClickListener((event) {
      // print('NOTIFICATION CLICK LISTENER CALLED WITH EVENT: $event');
      // this.setState(() {
      //   // print("Notifikasi di klik");
      //   // var data = event.notification.jsonRepresentation().replaceAll("\\n", "\n");

      // globals.appNavigator.currentState?.push(
      //   MaterialPageRoute(
      //     builder: (context) => NotificationPage(
      //       postId: event.notification.additionalData.toString(),
      //     ),
      //   ),
      // );
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            // Extract the postId from additionalData
            String? postId =
                event.notification.additionalData?['post_id'].toString();

            // If 'values' is a list, convert it to a string using join
            // String valuesAsString = event.notification.additionalData?['values']?.join(', ') ?? '';

            // Return the desired widget
            // print("Post Id nya adalah");
            // print(postId);
            return NotificationPage(
              postId: postId ?? 'defaultId',
              // values: valuesAsString,
            ); // Provide a default value if postId is null
          },
        ),
      );
      // print("Klik Mulai");
      // globals.appNavigator.currentState?.push(
      //   MaterialPageRoute(
      //     builder: (context) => NotificationPage(),
      //   ),
      // );

      //   _debugLabelString =
      //       "Setelah di Klik: \n${event.notification.jsonRepresentation().replaceAll("\\n", "\n")}";
      // });
      // print("all events: $event");
      // print("body is: ${event.notification.additionalData}");
      // print("Klik Selesai");
    });

    OneSignal.Notifications.addForegroundWillDisplayListener((event) {
      print(
          'NOTIFICATION WILL DISPLAY LISTENER CALLED WITH: ${event.notification.jsonRepresentation()}');

      /// Display Notification, preventDefault to not display
      event.preventDefault();

      /// Do async work

      /// notification.display() to display after preventing default
      event.notification.display();

      // this.setState(() {
      //   _debugLabelString =
      //       "Notification received in foreground notification: \n${event.notification.jsonRepresentation().replaceAll("\\n", "\n")}";
      // });
    });
  }
}
