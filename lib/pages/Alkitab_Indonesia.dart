import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AlkitabIndonesia extends StatefulWidget {
  const AlkitabIndonesia({super.key});

  @override
  State<AlkitabIndonesia> createState() => _AlkitabIndonesia();
}

class _AlkitabIndonesia extends State<AlkitabIndonesia> {

  final controller = WebViewController()
  ..setJavaScriptMode(JavaScriptMode.unrestricted)
  ..loadRequest(Uri.parse("https://alkitab.me/"));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alkitab Bahasa Indonesia"),
      ),
      body: WillPopScope(
        onWillPop: ()async{
          if(await controller.canGoBack()){
            controller.goBack();
            return false;
          }else{
            return true;
          }
          // return false;
        },
        child: WebViewWidget(controller: controller),
      ),
    );
  }
}
