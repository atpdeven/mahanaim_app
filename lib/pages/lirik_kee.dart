import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:mahanaim_app/model/KEE_Model.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class LirikKeee extends StatefulWidget {
  final KEE user;
  const LirikKeee({super.key, required this.user});

  @override
  State<LirikKeee> createState() => _LirikKeeeState();
}

class _LirikKeeeState extends State<LirikKeee> {
  late BannerAd bannerAd;
  bool isAdLoaded = false;
  // var adUnit = "ca-app-pub-2391162287708599/8243278870"; //Testing Ad Unit ID
  var adUnit = "ca-app-pub-3940256099942544/9214589741"; //Testing Ad Unit ID


  void initState() {
    super.initState();
    initBannerAd();
    final videoID = YoutubePlayer.convertUrlToId(widget.user.youtubeLink);
    _controller = YoutubePlayerController(
      initialVideoId: videoID!,
      flags: const YoutubePlayerFlags(autoPlay: false),
    );
  }

  initBannerAd() {
    bannerAd = BannerAd(
      size: AdSize.banner,
      adUnitId: adUnit,
      listener: BannerAdListener(
        onAdLoaded: (ad) {
          setState(() {
            isAdLoaded = true;
          });
        },
        onAdFailedToLoad: (ad, error) {
          ad.dispose();
          print(error);
        },
      ),
      request: AdRequest(),
    );

    bannerAd.load();
  }
  // final videoURL = widget.user.youtubeLink;

  late YoutubePlayerController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Lirik KEE No. ${widget.user.nomor}")),
      body: Padding(
        padding: EdgeInsets.all(15),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,

          //contains a single child which is scrollable

          //for horizontal scrolling
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.user.title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                widget.user.lyrics.replaceAll("\\n", "\n"),
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.user.youtubeLink != "youtubeLink")
                    SizedBox(
                      width: MediaQuery.of(context).size.width *
                          0.8, // Adjust the width as needed
                      height: MediaQuery.of(context).size.width * 0.45,
                      child: YoutubePlayer(
                        controller: _controller,
                        showVideoProgressIndicator: true,
                      ),
                    )
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: isAdLoaded
          ? SizedBox(
              height: bannerAd.size.height.toDouble(),
              width: bannerAd.size.width.toDouble(),
              child: AdWidget(ad: bannerAd),
            )
          : SizedBox(),
    );
  }
}
