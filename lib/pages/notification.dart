import 'package:flutter/material.dart';
import 'package:notification_permissions/notification_permissions.dart';

class Notification_Checker extends StatefulWidget {
  const Notification_Checker({super.key});

  @override
  State<Notification_Checker> createState() => _Notification_CheckerState();
}

class _Notification_CheckerState extends State<Notification_Checker>
    with WidgetsBindingObserver {
  late Future<String> permissionStatusFuture;

  var permGranted = "Diizinkan";
  var permDenied = "Ditolak";
  var permUnknown = "Tidak Diketahui";
  var permProvisional = "Provisional";

  @override
  void initState() {
    super.initState();
   
    permissionStatusFuture = getCheckNotificationPermStatus();

    WidgetsBinding.instance.addObserver(this);
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      setState(() {
        permissionStatusFuture = getCheckNotificationPermStatus();
      });
    }
  }

  /// Checks the notification permission status
  Future<String> getCheckNotificationPermStatus() {
    return NotificationPermissions.getNotificationPermissionStatus()
        .then((status) {
      switch (status) {
        case PermissionStatus.denied:
          return permDenied;
        case PermissionStatus.granted:
          return permGranted;
        case PermissionStatus.unknown:
          return permUnknown;
        case PermissionStatus.provisional:
          return permProvisional;
        default:
          return permDenied;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: const Text('Status Izin Notifikasi'),
        ),
        body: Center(
            child: Container(
          margin: EdgeInsets.all(20),
          child: FutureBuilder(
            future: permissionStatusFuture,
            builder: (context, snapshot) {
              // if we are waiting for data, show a progress indicator
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicator();
              }

              if (snapshot.hasError) {
                return Text(
                    'Error Ketika Mendeteksi Notifikasi: ${snapshot.error}');
              }

              if (snapshot.hasData) {
                var textWidget = Text(
                  "Status Notifikasi Anda adalah ${snapshot.data}",
                  style: TextStyle(fontSize: 20),
                  softWrap: true,
                  textAlign: TextAlign.center,
                );
                // The permission is granted, then just show the text
                if (snapshot.data == permGranted) {
                  return textWidget;
                }

                // else, we'll show a button to ask for the permissions
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    textWidget,
                    SizedBox(
                      height: 20,
                    ),
                    TextButton(
                      // color: Colors.amber,
                      child: Text("Ubah Status Notifikasi".toUpperCase()),
                      onPressed: () {
                        // show the dialog/open settings screen
                        NotificationPermissions.requestNotificationPermissions(
                                iosSettings: const NotificationSettingsIos(
                                    sound: true, badge: true, alert: true))
                            .then((_) {
                          // when finished, check the permission status
                          setState(() {
                            permissionStatusFuture =
                                getCheckNotificationPermStatus();
                          });
                        });
                      },
                    )
                  ],
                );
              }
              return Text("Belum ada Status Notifikasi");
            },
          ),
        )),
      ),
    );
  }
}
