import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mahanaim_app/pages/lirik_kee.dart';
import 'package:mahanaim_app/pages/tambah_kee.dart';
import 'package:mahanaim_app/model/KEE_Model.dart';

class DaftarKEE extends StatefulWidget {
  const DaftarKEE({super.key});

  @override
  State<DaftarKEE> createState() => _DaftarKEEState();
}

class _DaftarKEEState extends State<DaftarKEE> {
  final TextEditingController _searchController = TextEditingController();
  bool isSearchClicked = false;
  bool _isLoggedIn = false;
  String searchText = '';

  List<KEE> filteredItems = [];

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.authStateChanges().listen((user) {
      if (user != null) {
        setState(() {
          _isLoggedIn = true;
        });
      } else {
        setState(() {
          _isLoggedIn = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[300],
        title: isSearchClicked
            ? Container(
                height: 40,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15)),
                child: TextField(
                  controller: _searchController,
                  onChanged: (text) {
                    setState(() {
                      searchText = text.toLowerCase();
                    });
                  },
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    hintStyle: TextStyle(color: Colors.black),
                    border: InputBorder.none,
                    hintText: 'Cari Judul/Nomor KEE',
                  ),
                ),
              )
            : Text("Daftar KEE"),
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                isSearchClicked = !isSearchClicked;
                if (!isSearchClicked) {
                  _searchController.clear();
                  searchText = '';
                }
              });
            },
            icon: Icon(isSearchClicked ? Icons.close : Icons.search),
          )
        ],
      ),
      body: StreamBuilder<List<KEE>>(
        stream: readTitle(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Kesalahan Terjadi ${snapshot.error}');
          } else if (snapshot.hasData) {
            final users = snapshot.data!;
            filteredItems = users
                .where((user) =>
                    user.title.toLowerCase().contains(searchText) ||
                    user.nomor.contains(searchText))
                .toList();
            return ListView(
              children: filteredItems.map(buildUser).toList(),
              reverse: false,
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: _isLoggedIn
          ? FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Kee()),
                );
              },
              child: Icon(Icons.add),
            )
          : null, // Hide the button if not logged in
    );
  }

  Stream<List<KEE>> readTitle() =>
      FirebaseFirestore.instance.collection('kee').snapshots().map((snapshot) {
        var users =
            snapshot.docs.map((doc) => KEE.fromJson(doc.data())).toList();
        users.sort((a, b) {
          int nomorA = int.parse(a.nomor);
          int nomorB = int.parse(b.nomor);
          return nomorA.compareTo(nomorB);
        });
        return users;
      });

  Widget buildUser(KEE user) => ListTile(
        leading: CircleAvatar(
          child: Text('${user.nomor}'),
        ),
        title: Text(user.title),
        subtitle: Text('${user.lyrics.substring(0, 15)}...'),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => LirikKeee(
              user: user,
            ),
          ));
        },
      );
}
