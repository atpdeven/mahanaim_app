import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:mahanaim_app/navbar.dart';

class PendahinPermata extends StatefulWidget {
  const PendahinPermata({super.key});

  @override
  State<PendahinPermata> createState() => _PendahinPermataState();
}

class _PendahinPermataState extends State<PendahinPermata> {
  List<String> imageUrls = [];
  List<String> titles = [];
  List<String> rating = [];
  List<String> subtitle = [];
  List<String> instagram = [];
  List<String> tingkat = [];
  List<String> instagramLink = [];

  @override
  void initState() {
    super.initState();
    fetchImages();
  }

  Future<void> fetchImages() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot =
        await firestore.collection('pendahin').orderBy('tingkat').get();

    for (QueryDocumentSnapshot doc in querySnapshot.docs) {
      setState(() {
        titles.add(doc['title']);
        rating.add(doc['rating']);
        subtitle.add(doc['subtitle']);
        instagram.add(doc['instagram']);
        imageUrls.add(doc['image']);
        tingkat.add(doc['tingkat']);
        instagramLink.add(doc['instagramLink']);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text("Pendahin Permata"),
      ),
      body: Column(
        children: [
          Expanded(
            child: GridView.builder(
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
              itemCount: imageUrls.length,
              itemBuilder: ((context, index) {
                return Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                          imageUrls[index],
                        ),
                        fit: BoxFit.cover),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Text(
                                "  ${titles[index]}  ",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Spacer(),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Text(
                                "  ${rating[index]}  ",
                                style: TextStyle(
                                    color: Colors.orange,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Text(
                                "  ${subtitle[index]}  ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: GestureDetector(
                              onTap: () => launchUrl(
                                Uri.parse(
                                    instagramLink[index]),
                                mode: LaunchMode.externalApplication,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(8)),
                                child: Text(
                                  "  ${instagram[index]}  ",
                                  style: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}

// class Pendahin {
//   final String image;
//   final String title;
//   final String subtitle;
//   final double rating;
//   final String handphone;
//   final String instagramLink;

//   Pendahin(
//       {required this.image,
//       required this.title,
//       required this.subtitle,
//       required this.rating,
//       required this.handphone,
//       required this.instagramLink});
// }
