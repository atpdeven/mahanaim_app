import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mahanaim_app/model/KEE_Model.dart';

class Kee extends StatefulWidget {
  const Kee({super.key});

  @override
  State<Kee> createState() => _KeeState();
}

class _KeeState extends State<Kee> {
  final controller = TextEditingController();
  final nomorkee = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah KEE"),
        actions: [
          IconButton(
            onPressed: () {
              final title = controller.text;
              final nomor = nomorkee.text;
              createUser(title: title, nomor: nomor);
            },
            icon: Icon(Icons.add),
          )
        ],
      ),
      body: Column(
        children: [
          TextField(
            controller: controller,
          ),
          SizedBox(
            height: 30,
          ),
          TextField(
            controller: nomorkee,
          ),
        ],
      ),
    );
  }

  Future createUser({required String title, required String nomor}) async {
    final docUser = FirebaseFirestore.instance.collection('kee').doc();

    final user = KEE(
        id: docUser.id,
        title: title,
        lyrics: "Abdi Jepri Bangun",
        nomor: nomor,
        youtubeLink: "youtubeLink");

    final json = user.toJson();

    await docUser.set(json);
  }
}
