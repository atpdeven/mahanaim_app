import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

ThemeData lightMode = ThemeData(
  primaryColor: Colors.white,
  brightness: Brightness.light,
  primaryColorDark: Colors.black,
  canvasColor: Colors.white,
  appBarTheme: AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.dark),
  textTheme: TextTheme(),
);

ThemeData darkMode = ThemeData(
  primaryColor: Colors.black,
  primaryColorLight: Colors.black,
  brightness: Brightness.dark,
  primaryColorDark: Colors.black,
  indicatorColor: Colors.white,
  canvasColor: Colors.black,
  textTheme: TextTheme(
    displayLarge: TextStyle(color: Colors.white),
    displayMedium: TextStyle(color: Colors.white),
    bodyMedium: TextStyle(color: Colors.white),
  ),
  appBarTheme: AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.light),
);
