import 'package:flutter/material.dart';

class CartItem {
  String name;
  int price;
  int quantity;
  String image;

  factory CartItem.fromJson(Map<String, dynamic> json) => CartItem(
        name: json["name"],
        price: json["price"],
        quantity: json["quantity"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "price": price,
        "quantity": quantity,
        "image": image,
      };

  CartItem({
    required this.name,
    required this.price,
    required this.quantity,
    required this.image,
  });
}

class CartProvider with ChangeNotifier {
  List<CartItem> cartItems = [];

  void addtoCart(CartItem item) {
    cartItems.add(item);
    notifyListeners();
  }

  void setCartItems(List<CartItem> cartItems) {}
  void removeItem(CartItem item) {
    cartItems.remove(item);
    notifyListeners();
  }
}
